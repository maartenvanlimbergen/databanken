-- MVL
-- 28 februari 2012
-- Werken met INSERT
-- Bestandsnaam: BoekenInsertAlfredDenker.sql
use ModernWays;
insert into Boeken(
   Voornaam,
   Familienaam,
   Titel,
   Uitgeverij,
   Verschijningsjaar,
   InsertedBy
)
values (
   'Alfred',
   'Denker',
   'Onderweg in Zijn en Tijd',
   'Damon',
   '2017',
   'Jef Inghelbrecht'
);