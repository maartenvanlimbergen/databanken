-- MVL
-- 30/03/2018
-- oefeningen op 'between'

-- Oefening 1
use ModernWays;
select distinct Familienaam, 
				Voornaam
from Boeken
order by Familienaam asc, Voornaam;

-- Oefening 2
use ModernWays;
select distinct Familienaam, 
				Voornaam
from Boeken
	where (Familienaam like '%a%' or Familienaam like '%e%')
order by Familienaam asc, Voornaam;