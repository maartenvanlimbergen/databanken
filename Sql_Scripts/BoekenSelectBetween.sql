-- MVL
-- 30/03/2018
-- oefeningen op 'between'

-- Oefening 1
use ModernWays;
select  Voornaam, 
        Familienaam, 
        Titel
from Boeken
    where Verschijningsjaar between '1998' and '2001';

-- Oefening 2
use ModernWays;
select  Voornaam, 
        Familienaam, 
        Titel
from Boeken
    where Verschijningsjaar between '1900' and '2011'
    and Familienaam like 'B%'
order by Familienaam asc;

-- Oefening 3
use ModernWays;
select  Voornaam, 
        Familienaam,
        Titel, 
        Verschijningsjaar
from Boeken
    where Verschijningsjaar between '1900' and '2011'
    and Familienaam like '%B%' or 
        Familienaam like '%F%' or 
        Familienaam like '%A%'
order by Verschijningsjaar asc;

-- Oefening 4
use ModernWays;
select  Voornaam, 
        Familienaam,
        Titel, 
        Verschijningsjaar
from Boeken
    where Verschijningsjaar between '1990' and '2019' 
order by Verschijningsjaar asc;

-- Oefening 5
use ModernWays;
select  Voornaam, 
        Familienaam,
        Titel, 
        Verschijningsjaar
from Boeken
    where Verschijningsjaar between '0' and '2009' 
order by    Familienaam asc, 
            Voornaam asc;