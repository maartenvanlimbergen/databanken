-- MVL
-- 30 april 2018
-- naam van scriptbestand is WinkelCreate
-- volgt het patroon Tabelnaam + DDL statement

USE ModernWays;
DROP TABLE IF EXISTS `Winkels`;

-- de naam van de tabel in Pascalnotatie
CREATE TABLE Winkels(
    Naam NVARCHAR(50),
    Adres NVARCHAR(50),
    Postcode CHAR(4),
    Stad NVARCHAR(50),
    Telefoon NVARCHAR(50),
    -- hoeveel tekens heeft een vaste nummer?
    Id int auto_increment,
    constraint pk_Winkels_Id primary key(id)

);
ALTER TABLE Winkels AUTO_INCREMENT = 1;