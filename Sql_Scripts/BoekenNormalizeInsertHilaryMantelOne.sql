--MVL
--4/6/2018

use ModernWays;
insert into Boeken(
    Titel,
    Stad,
    Uitgeverij,
    Verschijningsjaar,
    Herdruk,
    Commentaar,
    Categorie,
    IdAuteur,
    InsertedBy)
Values
	(
   		'Wolf Hall',
   		'',
    	'Fourth Estate; First Picador Edition First Printing edition',
    	'2010',
    	'',
        'Goed boek',
        'Thriller',
        17,
        'JI');
