-- MVL
-- 14 maart 2018
-- Bestandsnaam: BoekenInsertAllJI.sql
-- gegevens komen uit de Korte Introductie
--
-- Opmerking
-- Als je een enkel aanhalingsteken in een string wil typen in SQL
-- moet je het enkel aanhalingstelen escapen omdat het enkel aanhalingsteken
-- eigenlijk een instructie is die aangeeft dat wat volgt een tekenreeks is.
-- Om een enkel aanhalingsteken te escapen ontdubbel je het bv:
-- 'Le siècle d''or'
-- Het eerste enkel aanhalingsteken begint de string, het tweede en derde geven
-- een enkel aanhalingsteken dat in de string wordt opgenomen en het vierde
-- sluit de string af.
-- Zet de use in commentaar om te voorkomen dat medecursisten gegevens
-- in jou tabel inserten. We gaan immers die insert scipts met elkaar delen.
use ModernWays;
insert into Boeken (
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Herdruk,
   Commentaar,
   Categorie,
   InsertedBy
)
values 
('Lucinda','Riley','De zeven zussen','','Xanders Uitgevers BV','2017','','nog te lezen','roman','FK'),
('Ali','Smith','herfst','','Prometheus','2018','','nog te lezen','roman','FK'),
('James M', 'Cain', 'The Postman Always Rings Twice', '', 'Alfred A. Knopf', '1934', '', 'nog te lezen','Misdaad', 'FK'),
('Diana','Gabaldon','Het vuur van de herfst','','Meulenhoff Boekerij','2015','','nog te lezen','roman','FK'),
('Diana','Gabaldon','De verre kust','','Meulenhoff Boekerij','2015','','goed boek','roman','FK'),
('Diana','Gabaldon','Sneeuw en as','','Meulenhoff Boekerij','2015','','nog te lezen','roman','FK'),
('Paolo','Cognetti','De acht bergen','','Bezige bij bv','2017','','nog te lezen','roman','FK'),
('Ernest','Hemingway','De oude man en de zee','','Strengholt united media','2015','','nog te lezen','roman','FK'),
('Lidia','Yuknavitch','Het boek Joan','','Lebowski','2018','','nog te lezen','FK'),
('Clarissa','Sehn','Vegetarisch uit het vuistje','','Zuidnederlandse Uitgeverij','2016','','interessante recepten','kookboek','FK'),


