use ModernWays;
insert into Personen (
    Voornaam,
    Familienaam,
    AanspreekTitel,
    Straat,
    Stad,
    Huisnummer,
    Biografie,
    Commentaar)
values (
    'Martha',
    'Jansens',
    'Mevrouw',
    'Plopperdeplopstraat',
    'Mechelen',
    '23',
    'Ze leefde lang en gelukkig.',
    'Goede schrijver');