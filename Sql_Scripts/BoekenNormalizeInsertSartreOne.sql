--MVL
--4/6/2018

use ModernWays;
insert into Boeken(
    Titel,
    Stad,
    Verschijningsjaar,
    Commentaar,
    Categorie,
    IdAuteur,
    InsertedBy)
values(
    'De Woorden',
    'Antwerpen',
    '1962',
    'Een zeer mooi boek.',
    'Roman',
    (select Id from Personen where
     Familienaam = 'Sartre' and Voornaam = 'Jean-Paul'),
    'JI');