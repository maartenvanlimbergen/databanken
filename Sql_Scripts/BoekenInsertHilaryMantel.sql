--mvl
--4/6/2018

use ModernWays;
insert into Boeken(
    Titel,
    Stad,
    Uitgeverij,
    Verschijningsjaar,
    Herdruk,
    Commentaar,
    Categorie,
    IdAuteur,
    InsertedBy)
Values
	(
   		'De geest geven',
   		'',
    	'Signatuur',
    	'2015',
    	'',
        '',
        'Roman',
        (select Id from Personen where Familienaam = 'Mantel'),
        'FK'),
    (
        'Broederschap',
   		'',
    	'Meridiaan',
    	'2015',
    	'',
        '',
        'Roman',
        (select Id from Personen where Familienaam = 'Mantel'),
        'FK'),
    (
        'De Moord op Margaret Thatcher',
   		'',
    	'Meridiaan',
    	'2015',
    	'',
        '',
        'Roman',
        (select Id from Personen where Familienaam = 'Mantel'),
        'FK');